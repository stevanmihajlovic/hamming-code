﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Hamming_Code
{
    public partial class HammingCodeForm : Form
    {
        private Hamming HammingCoder;
        private Stopwatch stopwatch;
        private string inputFilePath;
        
        public HammingCodeForm()
        {
            InitializeComponent();
            HammingCoder = new Hamming();
            matrixToTexbox(GenerationMatrixTextbox, HammingCoder.GeneratorMatrix);
            matrixToTexbox(ParityCheckMatrixTextbox, HammingCoder.ParityMatrix);
            stopwatch = new Stopwatch(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HammingCoder.textFile = false;
            decodeData(TextInputTextbox.Text);
        }

        private void decodeData(string data)
        {
            try
            {
                stopwatch.Restart();
                HammingCoder.ParseInput(data);
                BinaryTextboxBeforeCoding.Text = String.Empty;
                BinaryTextboxBeforeCoding.AppendText(HammingCoder.InputString + Environment.NewLine);
                HammingCoder.code();
                BinaryTextboxAfterCoding.Text = String.Empty;
                BinaryTextboxAfterCoding.AppendText(HammingCoder.OutputString + Environment.NewLine);
                stopwatch.Stop();
                MessageBox.Show("Vreme potrebno za kodiraje fajla: " + stopwatch.Elapsed.TotalSeconds.ToString() + " sekundi!");
            }

            catch (Exception exc)
            {
                MessageBox.Show("Unesite tekst maksimalne duzine od 20 karaktera, radi preglednosti!");
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            decodeData();
        }

        private void decodeData()
        {
            try
            {
                stopwatch.Restart();
                HammingCoder.generateErrors(PerBlockRadioButton.Checked, Convert.ToInt32(NumberOfErrorsTextbox.Text));
                BinaryTextboxAfterErrors.Text = String.Empty;
                BinaryTextboxAfterErrors.AppendText(HammingCoder.SentString + Environment.NewLine);

                HammingCoder.decode();
                BinaryTextboxAfterCorrecting.Text = String.Empty;
                BinaryTextboxAfterCorrecting.AppendText(HammingCoder.ReceivedString + Environment.NewLine);

                HammingCoder.parseOuput();

                if (HammingCoder.textFile)
                {
                    string extension = System.IO.Path.GetExtension(inputFilePath);
                    string path = inputFilePath.Substring(0, inputFilePath.Length - extension.Length) + "_decoded.txt";
                    //string path = Path.GetFileNameWithoutExtension(inputFilePath) + "_decoded.txt"; 
                    File.WriteAllText(path, HammingCoder.finalOutput);
                }
                else
                    textBox4.Text = HammingCoder.finalOutput;

                stopwatch.Stop();
                MessageBox.Show("Vreme potrebno za \"prenos\" i dekodiranje fajla: " + stopwatch.Elapsed.TotalSeconds.ToString() + " sekundi!");
            }
            catch (Exception exc)
            {
                MessageBox.Show("Nepravilan format broja gresaka!");
            }
        }

        private void matrixToTexbox(TextBox tB, int[,] m)
        {
            for (int i = 0; i < m.Length / 7; i++)
            {
                for (int j = 0; j < 7; j++)
                    tB.AppendText(m[i, j].ToString() + " ");
                if (i != m.Length / 7)
                    tB.AppendText("\n");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                inputFilePath = openFileDialog1.FileName;
                PathTextbox.Text = inputFilePath;
                HammingCoder.textFile = true;
                decodeData(File.ReadAllText(inputFilePath));
            }
            
        }
    }
}
