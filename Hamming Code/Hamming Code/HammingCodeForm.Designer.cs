﻿namespace Hamming_Code
{
    partial class HammingCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextInputTextbox = new System.Windows.Forms.TextBox();
            this.BinaryTextboxAfterCorrecting = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.GenerationMatrixTextbox = new System.Windows.Forms.TextBox();
            this.ParityCheckMatrixTextbox = new System.Windows.Forms.TextBox();
            this.InputLabel = new System.Windows.Forms.Label();
            this.PerBlockRadioButton = new System.Windows.Forms.RadioButton();
            this.PerTextRadioButton = new System.Windows.Forms.RadioButton();
            this.AmountOfErrorsGroupbox = new System.Windows.Forms.GroupBox();
            this.NumberOfErrorsTextbox = new System.Windows.Forms.TextBox();
            this.AfterCodingLabel = new System.Windows.Forms.Label();
            this.BinaryTextboxAfterCoding = new System.Windows.Forms.TextBox();
            this.BinaryTextboxAfterErrors = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BinaryTextboxBeforeCoding = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.PathTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.AmountOfErrorsGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextInputTextbox
            // 
            this.TextInputTextbox.Location = new System.Drawing.Point(11, 34);
            this.TextInputTextbox.Name = "TextInputTextbox";
            this.TextInputTextbox.Size = new System.Drawing.Size(451, 20);
            this.TextInputTextbox.TabIndex = 0;
            // 
            // BinaryTextboxAfterCorrecting
            // 
            this.BinaryTextboxAfterCorrecting.Location = new System.Drawing.Point(767, 179);
            this.BinaryTextboxAfterCorrecting.Multiline = true;
            this.BinaryTextboxAfterCorrecting.Name = "BinaryTextboxAfterCorrecting";
            this.BinaryTextboxAfterCorrecting.ReadOnly = true;
            this.BinaryTextboxAfterCorrecting.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BinaryTextboxAfterCorrecting.Size = new System.Drawing.Size(235, 331);
            this.BinaryTextboxAfterCorrecting.TabIndex = 2;
            this.BinaryTextboxAfterCorrecting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(11, 565);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(451, 20);
            this.textBox4.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(247, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(215, 22);
            this.button1.TabIndex = 3;
            this.button1.Text = "Kodiraj uneti tekst";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(593, 121);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Ubaci greske i dekodiraj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // GenerationMatrixTextbox
            // 
            this.GenerationMatrixTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenerationMatrixTextbox.Location = new System.Drawing.Point(767, 36);
            this.GenerationMatrixTextbox.Multiline = true;
            this.GenerationMatrixTextbox.Name = "GenerationMatrixTextbox";
            this.GenerationMatrixTextbox.ReadOnly = true;
            this.GenerationMatrixTextbox.Size = new System.Drawing.Size(106, 79);
            this.GenerationMatrixTextbox.TabIndex = 7;
            this.GenerationMatrixTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ParityCheckMatrixTextbox
            // 
            this.ParityCheckMatrixTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ParityCheckMatrixTextbox.Location = new System.Drawing.Point(895, 36);
            this.ParityCheckMatrixTextbox.Multiline = true;
            this.ParityCheckMatrixTextbox.Name = "ParityCheckMatrixTextbox";
            this.ParityCheckMatrixTextbox.ReadOnly = true;
            this.ParityCheckMatrixTextbox.Size = new System.Drawing.Size(107, 79);
            this.ParityCheckMatrixTextbox.TabIndex = 8;
            this.ParityCheckMatrixTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // InputLabel
            // 
            this.InputLabel.AutoSize = true;
            this.InputLabel.Location = new System.Drawing.Point(12, 15);
            this.InputLabel.Name = "InputLabel";
            this.InputLabel.Size = new System.Drawing.Size(129, 13);
            this.InputLabel.TabIndex = 9;
            this.InputLabel.Text = "Unesite tekst za kodiranje";
            // 
            // PerBlockRadioButton
            // 
            this.PerBlockRadioButton.AutoSize = true;
            this.PerBlockRadioButton.Checked = true;
            this.PerBlockRadioButton.Location = new System.Drawing.Point(12, 32);
            this.PerBlockRadioButton.Name = "PerBlockRadioButton";
            this.PerBlockRadioButton.Size = new System.Drawing.Size(106, 17);
            this.PerBlockRadioButton.TabIndex = 11;
            this.PerBlockRadioButton.TabStop = true;
            this.PerBlockRadioButton.Text = "Po Bloku (Max 7)";
            this.PerBlockRadioButton.UseVisualStyleBackColor = true;
            // 
            // PerTextRadioButton
            // 
            this.PerTextRadioButton.AutoSize = true;
            this.PerTextRadioButton.Location = new System.Drawing.Point(124, 32);
            this.PerTextRadioButton.Name = "PerTextRadioButton";
            this.PerTextRadioButton.Size = new System.Drawing.Size(96, 17);
            this.PerTextRadioButton.TabIndex = 12;
            this.PerTextRadioButton.Text = "U celom tekstu";
            this.PerTextRadioButton.UseVisualStyleBackColor = true;
            // 
            // AmountOfErrorsGroupbox
            // 
            this.AmountOfErrorsGroupbox.Controls.Add(this.NumberOfErrorsTextbox);
            this.AmountOfErrorsGroupbox.Controls.Add(this.PerBlockRadioButton);
            this.AmountOfErrorsGroupbox.Controls.Add(this.PerTextRadioButton);
            this.AmountOfErrorsGroupbox.Location = new System.Drawing.Point(500, 15);
            this.AmountOfErrorsGroupbox.Name = "AmountOfErrorsGroupbox";
            this.AmountOfErrorsGroupbox.Size = new System.Drawing.Size(230, 100);
            this.AmountOfErrorsGroupbox.TabIndex = 13;
            this.AmountOfErrorsGroupbox.TabStop = false;
            this.AmountOfErrorsGroupbox.Text = "Izaberite greske";
            // 
            // NumberOfErrorsTextbox
            // 
            this.NumberOfErrorsTextbox.Location = new System.Drawing.Point(64, 67);
            this.NumberOfErrorsTextbox.Name = "NumberOfErrorsTextbox";
            this.NumberOfErrorsTextbox.Size = new System.Drawing.Size(100, 20);
            this.NumberOfErrorsTextbox.TabIndex = 13;
            this.NumberOfErrorsTextbox.Text = "1";
            this.NumberOfErrorsTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AfterCodingLabel
            // 
            this.AfterCodingLabel.AutoSize = true;
            this.AfterCodingLabel.Location = new System.Drawing.Point(182, 77);
            this.AfterCodingLabel.Name = "AfterCodingLabel";
            this.AfterCodingLabel.Size = new System.Drawing.Size(0, 13);
            this.AfterCodingLabel.TabIndex = 15;
            // 
            // BinaryTextboxAfterCoding
            // 
            this.BinaryTextboxAfterCoding.Location = new System.Drawing.Point(239, 179);
            this.BinaryTextboxAfterCoding.Multiline = true;
            this.BinaryTextboxAfterCoding.Name = "BinaryTextboxAfterCoding";
            this.BinaryTextboxAfterCoding.ReadOnly = true;
            this.BinaryTextboxAfterCoding.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BinaryTextboxAfterCoding.Size = new System.Drawing.Size(223, 331);
            this.BinaryTextboxAfterCoding.TabIndex = 14;
            this.BinaryTextboxAfterCoding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BinaryTextboxAfterErrors
            // 
            this.BinaryTextboxAfterErrors.Location = new System.Drawing.Point(500, 179);
            this.BinaryTextboxAfterErrors.Multiline = true;
            this.BinaryTextboxAfterErrors.Name = "BinaryTextboxAfterErrors";
            this.BinaryTextboxAfterErrors.ReadOnly = true;
            this.BinaryTextboxAfterErrors.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BinaryTextboxAfterErrors.Size = new System.Drawing.Size(233, 331);
            this.BinaryTextboxAfterErrors.TabIndex = 16;
            this.BinaryTextboxAfterErrors.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Binarna reprezentacija teksta posle kodiranja:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(500, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Binarna reprezentacija teksta sa greskama:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(764, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Binarna reprezentacija teksta nakon dekodiranja:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Binarna reprezentacija teksta pre kodiranja:";
            // 
            // BinaryTextboxBeforeCoding
            // 
            this.BinaryTextboxBeforeCoding.Location = new System.Drawing.Point(11, 179);
            this.BinaryTextboxBeforeCoding.Multiline = true;
            this.BinaryTextboxBeforeCoding.Name = "BinaryTextboxBeforeCoding";
            this.BinaryTextboxBeforeCoding.ReadOnly = true;
            this.BinaryTextboxBeforeCoding.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BinaryTextboxBeforeCoding.Size = new System.Drawing.Size(188, 331);
            this.BinaryTextboxBeforeCoding.TabIndex = 21;
            this.BinaryTextboxBeforeCoding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(767, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Matrica generisanja G:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(895, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Matrica za proveru parnosti H:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(247, 121);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(215, 22);
            this.button3.TabIndex = 24;
            this.button3.Text = "Kodiraj tekstualni fajl";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PathTextbox
            // 
            this.PathTextbox.Location = new System.Drawing.Point(11, 88);
            this.PathTextbox.Name = "PathTextbox";
            this.PathTextbox.ReadOnly = true;
            this.PathTextbox.Size = new System.Drawing.Size(451, 20);
            this.PathTextbox.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 537);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Konacni dekodiran tekst:";
            // 
            // HammingCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 614);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PathTextbox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BinaryTextboxBeforeCoding);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BinaryTextboxAfterErrors);
            this.Controls.Add(this.AfterCodingLabel);
            this.Controls.Add(this.BinaryTextboxAfterCoding);
            this.Controls.Add(this.AmountOfErrorsGroupbox);
            this.Controls.Add(this.InputLabel);
            this.Controls.Add(this.ParityCheckMatrixTextbox);
            this.Controls.Add(this.GenerationMatrixTextbox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BinaryTextboxAfterCorrecting);
            this.Controls.Add(this.TextInputTextbox);
            this.Name = "HammingCodeForm";
            this.Text = "HammingCode";
            this.AmountOfErrorsGroupbox.ResumeLayout(false);
            this.AmountOfErrorsGroupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextInputTextbox;
        private System.Windows.Forms.TextBox BinaryTextboxAfterCorrecting;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox GenerationMatrixTextbox;
        private System.Windows.Forms.TextBox ParityCheckMatrixTextbox;
        private System.Windows.Forms.Label InputLabel;
        public System.Windows.Forms.RadioButton PerBlockRadioButton;
        public System.Windows.Forms.RadioButton PerTextRadioButton;
        private System.Windows.Forms.GroupBox AmountOfErrorsGroupbox;
        private System.Windows.Forms.Label AfterCodingLabel;
        private System.Windows.Forms.TextBox BinaryTextboxAfterCoding;
        private System.Windows.Forms.TextBox NumberOfErrorsTextbox;
        private System.Windows.Forms.TextBox BinaryTextboxAfterErrors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BinaryTextboxBeforeCoding;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox PathTextbox;
        private System.Windows.Forms.Label label7;
    }
}

