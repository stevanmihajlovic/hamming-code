﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamming_Code
{
    public class Hamming
    {
        public int[,] ParityMatrix;//matrica za kontrolu parnosti
        public int[,] GeneratorMatrix;//matrica generisanja
        public int[] Input;//niz integera u kome smestamo binarnu reprezentaciju teksta koji kodiramo, umnozak broja 8
        public string InputString;//string u kome se cuva binarna reprezentacija teksta koji kodiramo, sa space karakterom na svaka 4 bita, odnosno "\n" na svaka 8 bita
        public int[][] Output;//matrica integera u kome smestamo binarnu reprezentaciju kodiranog teksta
        public string OutputString;//string u kome se cuva binarna reprezentacija teksta koji kodiramo nakon kodiranja, sa space karakterom na svaka 7 bitova, odnosno "\n" na svaka 14 bitova
        public int[][] Sent;//matrica integera u kome smestamo binarnu reprezentaciju teksta koji kodiramo, nakon umetanja gresaka
        public string SentString;//string u kome se cuva binarna reprezentacija teksta koji kodiramo, nakon umetanja gresaka
        public int[][] Received;//matrica integera u kome smestamo binarnu reprezentaciju teksta koji kodiramo, nakon dekodiranja
        public string ReceivedString;//string u kome se cuva binarna reprezentacija teksta koji kodiramo, nakon dekodiranja, parsirana na slogove
        public string ReceivedStringForParse;//string u kome se cuva binarna reprezentacija teksta koji kodiramo, nakon dekodiranja, bez parsiranja
        public string finalOutput;//string koju sadrzi dekodiranu pocetnu poruku, sa ili bez greske
        public bool perBlock = true;//da li unosimo greske po bloku ili po celom tekstu
        public int errors;//broj gresaka
        public bool textFile = false;//da li radimo sa tekstualnim fajlom ili unosom
        public Hamming()   //Inicijalizacija Parity Check i Generator matrice za (7,4) Hammingov kod
        {
            ParityMatrix = new int[3, 7] { { 1, 1, 0, 1, 1, 0, 0 },
                                           { 1, 0, 1, 1, 0, 1, 0 },
                                           { 1, 1, 1, 0, 0, 0, 1 } };

            GeneratorMatrix = new int[4, 7] { { 1, 0, 0, 0, 1, 1, 1 },
                                              { 0, 1, 0, 0, 1, 0, 1 },
                                              { 0, 0, 1, 0, 0, 1, 1 },
                                              { 0, 0, 0, 1, 1, 1, 0 } };
        }
        public void ParseInput(string s)//f-ja koja zapocinje obradu teksta, prebacuje string u binarno vrednosti po ASCII-ju
        {
            if (!textFile && s.Length > 20)
                throw new Exception();

            OutputString = String.Empty;
            ReceivedStringForParse = String.Empty;
            SentString = String.Empty;

            byte[] asciiBytes = Encoding.ASCII.GetBytes(s);
            BitArray bits = new BitArray(asciiBytes);
            Input = new int[bits.Length];
            
            Output = new int[s.Length * 2][];
            Sent = new int[s.Length * 2][];
            for (int i = 0; i < Output.Length; i++)
            {
                Output[i] = new int[7];
                Sent[i] = new int[7];
            }
            int j = 7;
            int k = 0;
            for (int i = 0; i != bits.Length; i += 1)//zbog specificnog ucitavanja bitova, popunjavamo niz Input na sledeci nacin
            {
                Input[8 * k + j] = (bits[i] ? 1 : 0);
                if (j==0)
                {
                    k++;
                    j = 8;
                }
                j--;                
            }
            InputString = intToStringNew(Input, 8);
        }
        public String intToStringNew(int[] array,int length)//binarni prikaz razdvojen razmacima i novim redovima              
        {
            string s = "";
            for (int i = 0; i < array.Length; i++)
            {
                if (i > 0 && i % length == 0)
                    s += Environment.NewLine;
                else if (i > 0 && i % (length / 2) == 0)
                    s += "\t";
                s += array[i];
            }
            return s;
        }
        public void code()//popunjava Input matricu i Input string
        {
            int i = 0;
            int[] block = new int[4];
            while (i < Input.Length)
            {
                for (int j = 0; j < 4; j++)
                    block[j] = Input[i++];
                codeBlock(block, (i - 1) / 4);
            }            
            OutputString = matrixToString(Output, 2);
        }
        public void codeBlock(int[] block, int outputRow)//punimo Output matricu
        {
            for (int i = 0; i < 7; i++)
                Output[outputRow][i] = multiplyVerticesCode(block, i);
        }

        public int multiplyVerticesCode(int[] block, int generatorColumn)//mnozenje vrste Output matrice sa kolonom generatorske matrice
        {
            int result = 0;
            for (int i = 0; i < 4; i++)
            {
                result += block[i] * GeneratorMatrix[i, generatorColumn];
            }
            result = result % 2;
            return result;
        }

        private String matrixToString(int[][] matrix, int length)//prebacuje matricu u string, length je koliko stvari ide pre novog reda//NOVA F-JA
        {
            string s = "";
            for (int i = 0; i < matrix.Length; i++)
            {
                if (i > 0 && i % length == 0)
                    s += Environment.NewLine;
                else if (i > 0 && i % length == (length / 2))
                    s += "\t";
                s += intToString(Output[i]);                
            }
            return s;
        }
        public String intToString(int[] array)//prebacuje vrstu matrice u string
        {
            string s = "";
            for (int i = 0; i < array.Length; i++)
                s += array[i];
            return s;
        }

        public void generateErrors(bool check, int numberOfErrors)//generise greske, ima random, niz koji pamti randome, i zavisi od boola (blok ili ceo tekst)
        {
            ReceivedStringForParse = String.Empty;
            ReceivedString = String.Empty;

            perBlock = check;
            List<int> temp = new List<int>();
            if (check && numberOfErrors > 7)
                throw new Exception();

            errors = numberOfErrors;          
            Random random = new Random();
            int randomNumber, j;

            for (int k = 0; k < Output.Length; k++)//moramo da "resetujumo" matricu pre umetanja gresaka
                for (int l = 0; l < 7; l++)
                    Sent[k][l] = Output[k][l];

            if(perBlock)
            {
                for (int i = 0; i < Sent.Length; i++)
                {
                    j = 0;
                    temp.Clear();
                    while (j < numberOfErrors)
                    {
                        randomNumber = random.Next(0, 7);
                        if (!temp.Contains(randomNumber))
                        {
                            Sent[i][randomNumber] = (Sent[i][randomNumber] == 1 ? 0 : 1);
                            temp.Add(randomNumber);
                            j++;
                        }
                    }
                }
            }
            else
            {
                int numberOfColumns = Sent[0].Length;
                j = 0;
                while (j < numberOfErrors)
                {
                    randomNumber = random.Next(0, Sent.Length * numberOfColumns);
                    if (!temp.Contains(randomNumber))
                    {
                        Sent[randomNumber / numberOfColumns][randomNumber % numberOfColumns] = 
                            (Sent[randomNumber / numberOfColumns][randomNumber % numberOfColumns] == 1 ? 0 : 1);
                        temp.Add(randomNumber);
                        j++;
                    }
                }                
            }
            SentString = matrixToString(Sent, 2);
        }
        public void decode()//received matrica je duzine 4 po vrstama
        {
            int[] syndrome = new int[3];

            Received = new int[Sent.Length][];
            for (int i = 0; i < Sent.Length; i++)
                Received[i] = new int[4];

            for (int i = 0; i < Sent.Length; i++)
            {
                syndrome = calculateSyndrom(Sent[i]);
                if (syndrome.SequenceEqual(new int[] { 0, 0, 0 }))
                {
                    decodeBlock(Sent[i], Received[i], -1);
                }
                else
                    for (int j = 0; j < 7; j++)
                        if (syndrome.SequenceEqual(new int[] { ParityMatrix[0, j], ParityMatrix[1, j], ParityMatrix[2, j] }))
                        {
                            decodeBlock(Sent[i], Received[i], j);
                            break;
                        }
            }

            parseReceivedString();
        }
        public int[] calculateSyndrom(int[] sentBlock)//racuna sindrom
        {
            int[] syndrome = new int[3];
            for (int i = 0; i < 3; i++)
            {
                syndrome[i] = multiplyVerticesDecode(sentBlock, i);
            }
            return syndrome;
        }
        public int multiplyVerticesDecode(int[] block, int parityCheckRow)//racunamo da li je sindrom razlicit od 0
        {
            int result = 0;
            for (int i = 0; i < 7; i++)
            {
                result += block[i] * ParityMatrix[parityCheckRow, i];
            }
            result = result % 2;
            return result;
        }
        private void decodeBlock(int[] sendBlock, int[] receivedBlock, int index)//ono sa temp-om sto kratimo ili menjamo odgovarajuci bit
        {
            int[] tempReceiver = new int[7];
            Array.Copy(sendBlock, tempReceiver, 7);
            if (index != -1)
                tempReceiver[index] = (tempReceiver[index] == 1 ? 0 : 1);
            Array.Copy(tempReceiver, receivedBlock, 4);
            ReceivedStringForParse += intToString(receivedBlock);
        }
        public void parseReceivedString()//pravi string za ispis u 4. textbox
        {
            int i;
            for (i = 0; i < ReceivedStringForParse.Length / 8 - 1; i++)
            {
                ReceivedString += ReceivedStringForParse.Substring(i * 8, 4) + "\t"
                    + ReceivedStringForParse.Substring(i * 8 + 4, 4) + Environment.NewLine;
            }

            ReceivedString += ReceivedStringForParse.Substring(i * 8, 4) + "\t"
                    + ReceivedStringForParse.Substring(i * 8 + 4, 4);
        }
        public void parseOuput()//OVO NAM IZGLEDA NE TREBA, PROVERITI SA DUSANOM
        {
            var data = GetBytesFromBinaryString(ReceivedStringForParse);
            finalOutput = Encoding.ASCII.GetString(data);
        }

        public Byte[] GetBytesFromBinaryString(String binary)// KAO I OVO
        {
            var list = new List<Byte>();

            for (int i = 0; i < binary.Length; i += 8)
            {
                String t = binary.Substring(i, 8);

                list.Add(Convert.ToByte(t, 2));
            }

            return list.ToArray();
        }

        
    }
}
